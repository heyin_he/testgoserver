# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys
import oss2
import datetime
import ConfigParser
import zipfile
import shutil
import hashlib
import time
import json
import pytoml as toml

ROOT = os.path.dirname(os.path.realpath(__file__))


bucket = "leshusanguo-agent-admin"
AccessKeyId="LTAI5tF8UZigUchty6DHVTbK"
AccessKeySecret="g86K0kfLGIutLNZykKcqT7gKrhgHUU"
Endpoint="oss-cn-beijing.aliyuncs.com"


def percentage(consumed_bytes, total_bytes):
    if total_bytes:
        rate = int(100 * (float(consumed_bytes) / float(total_bytes)))
        print('\r{0}% '.format(rate), end='')
        sys.stdout.flush()


print('连接阿里云oss服务器')
print(AccessKeyId)
print(AccessKeySecret)
auth = oss2.Auth(AccessKeyId, AccessKeySecret)
service = oss2.Service(auth, Endpoint)


bucket = oss2.Bucket(auth, Endpoint, bucket)

version_dir = './dist'


# 遍历所有的子目录进行上传
for (root, dirs, files) in os.walk(version_dir):
    for filename in files:
        fpath = os.path.join(root, filename)
        fdir, fname = os.path.split(fpath)
        if not os.path.isdir(fpath) and not fname.startswith("."):
            subpath = fpath.replace(version_dir, "")
            if subpath.startswith("/"):
                subpath = subpath.replace("/", '', 1)

            with open(fpath, 'rb') as fileobj:
                bucket.put_object(subpath, fileobj.read(),
                                  progress_callback=percentage)
            pass