import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import {GETUSERLIST, GETAGENTLIST, CHECKUSERNAME, SETAGENT, DELAGENT, FREEZE, UNFREEZE, GETAGENTINFO, GETAGENTCHILDLIST} from "@/api/member/uri";
const httpInstance = new HttpService(baseUrl);

export function getUserList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETUSERLIST, param);
}

export function getAgentList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETAGENTLIST, param);
}

export function checkUserName(param: object): Promise<AjaxResponse> {
    return httpInstance.post(CHECKUSERNAME, param);
}

export function setAgent(param: object): Promise<AjaxResponse> {
    return httpInstance.post(SETAGENT, param);
}

export function delAgent(param: object): Promise<AjaxResponse> {
    return httpInstance.post(DELAGENT, param);
}

export function Freeze(param: object): Promise<AjaxResponse> {
    return httpInstance.post(FREEZE, param);
}

export function UnFreeze(param: object): Promise<AjaxResponse> {
    return httpInstance.post(UNFREEZE, param);
}

export function getAgentInfo(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETAGENTINFO, param);
}

export function getAgentChildList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETAGENTCHILDLIST, param);
}
