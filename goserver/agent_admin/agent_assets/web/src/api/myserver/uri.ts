export const GETSERVERLIST = '/api/getserverlist';
export const ADDSERVER = '/api/addserver';
export const UPDATESERVER = '/api/updateserver';
export const GETSERVERITEMLIST = '/api/getserveritemlist';
export const OPENSERVER = '/api/openserver';
