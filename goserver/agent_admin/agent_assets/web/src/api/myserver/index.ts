import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import { GETSERVERLIST, ADDSERVER, UPDATESERVER, OPENSERVER, GETSERVERITEMLIST } from "@/api/myserver/uri";
const httpInstance = new HttpService(baseUrl);

export function getServerList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETSERVERLIST, param);
}

export function addServer(param: object): Promise<AjaxResponse> {
    return httpInstance.post(ADDSERVER, param);
}

export function updateServer(param: object): Promise<AjaxResponse> {
    return httpInstance.post(UPDATESERVER, param);
}

export function openServer(param: object): Promise<AjaxResponse> {
    return httpInstance.post(OPENSERVER, param);
}

export function getServerItemList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETSERVERITEMLIST, param);
}


