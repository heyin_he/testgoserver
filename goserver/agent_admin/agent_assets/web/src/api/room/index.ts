import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import { CREATEROOM, GETROOM, SETROOM } from './uri';
const httpInstance = new HttpService(baseUrl);

export function getRoom(): Promise<AjaxResponse> {
    return httpInstance.get(GETROOM);
}

export function setRoom(param: object): Promise<AjaxResponse> {
    return httpInstance.post(SETROOM,param);
}

export function createRoom(param: object): Promise<AjaxResponse> {
    return httpInstance.post(CREATEROOM,param);
}