import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import { GETREBATELIST } from "@/api/rebate/uri";
const httpInstance = new HttpService(baseUrl);

export function getRebateList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETREBATELIST, param);
}
