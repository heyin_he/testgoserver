import { HttpService } from '../util';
import { baseUrl } from '../server';
import { GETHOMEDATA, ONLINE_USER, GETAGENTHOMEDATA } from './uri';
import { AjaxResponse } from '@/type';

const httpInstance = new HttpService(baseUrl);

export function getHomeData(): Promise<AjaxResponse> {
    return httpInstance.post(GETHOMEDATA);
}

export function getAgentHomeData(param : any): Promise<AjaxResponse> {
    return httpInstance.post(GETAGENTHOMEDATA, param);
}

export function getOnlineUser(): Promise<AjaxResponse> {
    return httpInstance.post(ONLINE_USER);
}
