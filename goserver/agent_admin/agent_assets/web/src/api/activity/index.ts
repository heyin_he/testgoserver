import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import { GETACTIVITYLIST, UPDATEACTIVITY } from "@/api/activity/uri";
const httpInstance = new HttpService(baseUrl);

export function getActivityList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETACTIVITYLIST, param);
}

export function updateActivity(param: object): Promise<AjaxResponse> {
    return httpInstance.post(UPDATEACTIVITY, param);
}
