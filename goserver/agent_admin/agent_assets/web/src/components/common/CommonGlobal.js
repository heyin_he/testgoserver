export class CommonGlobal {
  //将国际时间转为时间轴方法
  static formatTen(num) {
    return num > 9 ? (num + "") : ("0" + num);
  }
  static formatDate(date1) {
    var date = new Date(date1);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let hour = date.getHours();
    let minute = date.getMinutes();
    let second = date.getSeconds();
    return year + "-" + this.formatTen(month) + "-" + this.formatTen(day) + " " +
      hour +
      ":" +
      minute +
      ":" +
      second;
  }

}