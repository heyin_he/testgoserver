import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { getMyWithdrawList, doWithdraw } from '@/api/withdraw';
import store from '@/store';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class WithdrawController extends Vue {

    modal: boolean = false
    modal1: boolean = false
    modal2: boolean = false

    columns: Array<TableHeader> = [
        {
            title: '用户ID',
            slot: 'uid',
            minWidth: 80
        },
        {
            title: '提现金额',
            slot: 'money',
            minWidth: 50
        },
        {
            title: '收款方式',
            slot: 'type',
            minWidth: 50
        },
        {
            title: '收款账户',
            slot: 'account',
            minWidth: 50
        },
        {
            title: '税金',
            slot: 'taxes',
            minWidth: 50
        },

        {
            title: '审核状态',
            slot: 'status',
            minWidth: 50,
            filters: [
                {
                    label: '待审核',
                    value: 0
                },
                {
                    label: '审核通过',
                    value: 1
                },
                {
                    label: '审核拒绝',
                    value: 2
                },
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 0) {
                    return row.status === 0;
                } else if (value === 1) {
                    return row.status === 1;
                } else if (value === 2) {
                    return row.status === 2;
                }
            }
        },
        {
            title: '备注信息',
            slot: 'content',
            minWidth: 50
        },
        {
            title: '提现时间',
            slot: 'created_at',
            minWidth: 50
        },
        {
            title: '审核时间',
            slot: 'updated_at',
            minWidth: 50
        }
    ];

    total: number = 0
    data: Array<any> = []

    formCustom: any = {
        money: 0.00,
        content: "",
    }

    isadd: boolean = false

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }


    created() {
        this.getList()
    }

    getList() {
        var param = { ...this.pageValue, ...{ agent_id: (store.state as any).user.agent_id } };
        getMyWithdrawList(param).then(resp => {
            if (resp.errcode == 0) {
                this.data = resp.data.list
                this.total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }


    show(id: number) {
        this.modal1 = true
    }


    add() {
        this.isadd = true
        this.modal = true
    }


    ok() {
        this.formCustom.money = parseFloat(this.formCustom.money)
        var param = { ...this.formCustom, ...{ agent_id: (store.state as any).user.agent_id } };
        console.log("param = ", param)
        doWithdraw(param).then(resp => {
            console.log("resp", resp)
            if (resp.errcode == 0) {
                this.$Message.success(resp.errmsg);
                this.getList()
                this.modal = false
            } else {
                this.$Message.error(resp.errmsg)
            }
        })
    }

    cancel() {
        this.modal = false
        this.formCustom = {
            money: 0.00,
            content: "",
        }
    }

    page(page: number) {
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        this.pageValue.limit = limit
        this.getList()
    }

}
