import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { createRoom, getRoom, setRoom } from '@/api/room';
interface Obj {
  [key: string]: string;
}

interface NumberObj {
  [key: string]: number;
}

@Component({
  components: { CommonCardTitle, PopConfirm }
})
export default class RoomController extends Vue {

  modal: boolean = false
  ruleValidate: any = {
    user_count: [
      { tyep: "number", min: 1, max: 100, message: '房间人数在1-100之间', trigger: 'blur' },
      { required: true, message: '请输入房间人数', trigger: 'blur' }
    ],
  }

  columns: Array<TableHeader> = [
    {
      title: '房间ID',
      slot: 'id',
      minWidth: 100
    },
    {
      title: '房间人数',
      slot: 'user_count',
      minWidth: 150
    },
    {
      title: '操作',
      slot: 'action',
      width: 150,
      align: 'center'
    }
  ];

  total: number = 0
  data: Array<any> = []

  formCustom: any = {
    id: "",
    user_count: 0
  }

  isadd: boolean = false

  pageValue: NumberObj = {
    page: 1,
    limit: 10,
  }

  created() {
    this.getList()
  }

  getList() {
    getRoom().then(resp => {
      if (resp.errcode == 0) {
        console.log("resp=======", resp)
        this.data = resp.data.list
        // this.total = resp.data.total
      } else {
        this.$Message.error(resp.errmsg)
      }
    });
  }


  show(row: Obj) {
    this.formCustom.id = row.id
    this.formCustom.user_count = row.user_count
    this.isadd = false
    this.modal = true
  }


  add() {
    this.isadd = true
    this.modal = true
    this.formCustom.user_count = 0
  }

  remove(id: number) {
    return () => {

    }
  }

  ok() {
    if (this.isadd) {
      (this.$refs.ruleValidate as Vue & { validate: Function }).validate((valid: any) => {
        console.log(valid)
        if (valid) {
          createRoom(this.formCustom).then(resp => {
            console.log("resp", resp)
            if (resp.errcode == 0) {
              this.$Message.success(resp.errmsg);
              this.getList()
              this.modal = false
            } else {
              this.$Message.error(resp.errmsg)
            }
          })
        } else {
          return
          // this.$Message.error("房间人数在1-100之间")
        }
      })
    } else {
      (this.$refs.ruleValidate as Vue & { validate: Function }).validate((valid: any) => {
        console.log(valid)
        if (valid) {
          setRoom(this.formCustom).then(resp => {
            console.log("resp", resp)
            if (resp.errcode == 0) {
              this.$Message.success(resp.errmsg);
              this.getList()
              this.modal = false
            } else {
              this.$Message.error(resp.errmsg)
            }
          })
        } else {
          return
          // this.$Message.error("房间人数在1-100之间")
        }
      })
    }
  }

  cancel() {
    this.isadd = false
    this.modal = false
    this.formCustom.id = ""
    this.formCustom.user_count = ""
  }

  page(page: number) {
    this.pageValue.page = page
    this.getList()
  }

  pageSize(limit: number) {
    this.pageValue.limit = limit
    this.getList()
  }
}
