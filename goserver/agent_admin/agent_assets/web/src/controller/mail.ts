import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { getMailList, sendMail } from '@/api/mail';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class MailController extends Vue {

    modal: boolean = false

    columns: Array<TableHeader> = [
        {
            title: '邮件标题',
            slot: 'title',
            minWidth: 80
        },
        {
            title: '内容',
            slot: 'content',
            minWidth: 100
        },
        {
            title: '发送人',
            slot: 'sender',
            minWidth: 100
        },
        {
            title: '接收人',
            slot: 'receiver',
            minWidth: 100
        },
        {
            title: '发送时间',
            slot: 'created_at',
            minWidth: 120
        },
        {
            title: '操作',
            slot: 'action',
            width: 150,
            align: 'center'
        }
    ];

    total: number = 0
    data: Array<any> = []

    formCustom: any = {
        title: '',
        content: '',
        rewards: [{ key: '', value: 0 }],
        sender: "系统",
        receiver: ''
    }

    config: any = {
        coin: "铜钱",
        gold: "金砖",
        box_lv1: "低级宝箱",
        box_lv2: "初级宝箱",
        box_lv3: "中级宝箱",
        box_lv4: "高级宝箱",
        box_lv5: "顶级宝箱",
        jingtie: "玄铁",
        mengyanjingshi: "梦魇晶石",
        jinjieshi: "紫石",
        zhaomuling: "招募令",
        xianshenmenpiao: "咸神门票",
        nengliang: "能量",
        huoba_lv1: "木机关人",
        huoba_lv2: "铜机关人",
        huoba_lv3: "金机关人",
        zhenzhu: "珍珠",
        juntuanbi: "俱乐部币",
        pifubi: "皮肤币",
        saodangmotan: "扫荡魔毯",
        baiyu: "白玉",
        caiyu: "彩玉",
        banshou: "扳手",
        chunsun: "春笋",
        xiaoyugan: "小鱼干",
        shendeng: "神灯",
        yugan_lv1: "普通鱼竿",
        yugan_lv2: "黄金鱼竿",
        zhishibi: "知识币",
        zhuweibi: "助威币",
        libao_coin: "铜钱礼包",
        libao_gold: "金砖礼包",
        libao_jinjieshi: "紫石礼包",
        libao_jingtie: "玄铁礼包",
        libao_mengyanjingshi: "梦魇精石礼包",
        libao_wujiang_zi1: "随机紫色武将礼包",
        libao_wujiang_cheng1: "随机橙色武将礼包",
        libao_wujiang_cheng2: "万能橙色武将礼包",
        libao_wujiang_hong1: "随机红色武将礼包",
        libao_wujiang_hong2: "万能红色武将礼包",
        libao_jubaoding_lv1: "聚宝鼎",
        libao_jubaoding_lv2: "豪华聚宝鼎",
        libao_baiyu: "白玉福袋",
        libao_banshou: "扳手福袋",
        card_101: "司马懿碎片",
        card_102: "郭嘉碎片",
        card_103: "关羽碎片",
        card_104: "诸葛亮碎片",
        card_105: "周瑜碎片",
        card_106: "太史慈碎片",
        card_107: "吕布碎片",
        card_108: "华佗碎片",
        card_109: "甄姬碎片",
        card_110: "黄月英碎片",
        card_111: "孙策碎片",
        card_112: "贾诩碎片",
        card_113: "曹仁碎片",
        card_114: "姜维碎片",
        card_115: "孙坚碎片",
        card_116: "公孙瓒碎片",
        card_202: "荀彧碎片",
        card_203: "典韦碎片",
        card_207: "鲁肃碎片",
        card_208: "陆逊碎片",
        card_209: "甘宁碎片",
        card_210: "貂蝉碎片",
        card_211: "董卓碎片",
        card_212: "张角碎片",
        card_215: "许褚碎片",
        card_220: "马岱碎片",
        card_223: "蔡文姬碎片",
        card_227: "颜良碎片",
        card_228: "文丑碎片",
        card_301: "周泰碎片",
        card_302: "许攸碎片",
        card_303: "于禁碎片",
        card_304: "张星彩碎片",
        card_305: "关银屏碎片",
        card_306: "关平碎片",
        card_307: "程普碎片",
        card_308: "张昭碎片",
        card_312: "邢道荣碎片",
        card_313: "祝融夫人碎片",
        card_314: "孟获碎片",
    }

    configItems: any = [

    ];

    isadd: boolean = false

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }

    created() {
        this.configItems = Object.entries(this.config).map(([value, label]) => ({ value, label }));
        this.getList()
    }

    getList() {

        getMailList(this.pageValue).then(resp => {
            if (resp.errcode == 0) {
                this.data = resp.data.list
                this.total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }


    show(row: any) {
        console.log("show row = ", row)
        this.isadd = false
        this.modal = true
        this.formCustom = row
    }


    sendmail() {
        this.isadd = true
        this.modal = true
    }

    remove(row: any) {
        return () => {
        }
    }

    changeStatus(row: any, status: number) {
        return () => {
        }
    }


    ok() {
        var obj = { ...this.formCustom }
        obj.receiver = parseInt(obj.receiver)
        if(this.isadd){
            sendMail(obj).then(resp => {
                if (resp.errcode == 0) {
                    this.$Message.success(resp.errmsg);
                    this.cancel()
                    this.getList()
                } else {
                    this.$Message.error(resp.errmsg)
                }
            });
        }
        this.modal = false
    }

    cancel() {
        this.modal = false
        this.formCustom = {
            title: '',
            content: '',
            rewards: [{ key: '', value: 0 }],
            sender: "系统",
            receiver: ''
        }
    }

    page(page: number) {
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        this.pageValue.limit = limit
        this.getList()
    }

    addProduct() {
        this.formCustom.rewards.push({ key: '', value: 1 });
    }

    removeProduct(index: any) {
        this.formCustom.rewards.splice(index, 1);
    }
}