import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import store from '@/store';
import { GetMyTiXianList, ApplyTiXian, UpdateApplyTixian } from '@/api/agent';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class TiXianController extends Vue {

    modal: boolean = false
    modal1: boolean = false
    modal2: boolean = false

    columns: Array<TableHeader> = [
        {
            title: '用户ID',
            slot: 'agent_id',
            minWidth: 80
        },
        {
            title: '收款方式',
            slot: 'type',
            minWidth: 50,
            filters: [
                {
                    label: '支付宝',
                    value: 1
                },
                {
                    label: '银行卡',
                    value: 2
                },
            ],
        },
        {
            title: '账号信息',
            slot: 'account',
            minWidth: 100
        },
        {
            title: '审核状态',
            slot: 'status',
            minWidth: 50,
            filters: [
                {
                    label: '待审核',
                    value: 0
                },
                {
                    label: '审核通过',
                    value: 1
                },
                {
                    label: '审核拒绝',
                    value: 2
                },
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 0) {
                    return row.status === 0;
                } else if (value === 1) {
                    return row.status === 1;
                } else if (value === 2) {
                    return row.status === 2;
                }
            }
        },
        {
            title: '提审时间',
            slot: 'created_at',
            minWidth: 50
        },
        {
            title: '审核时间',
            slot: 'updated_at',
            minWidth: 50
        },
        {
            title: '操作',
            slot: 'action',
            minWidth: 80
        }
    ];

    total: number = 0
    data: Array<any> = []
    isadd: boolean = false

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }

    formCustom: any = {
        agent_id : "",
        type : "",
        account: ""
    }

    created() {
        this.getList()
    }

    getList() {
        GetMyTiXianList({agent_id : (store.state as any).user.agent_id}).then(resp => {
            if (resp.errcode == 0) {
                if(resp.data.list && resp.data.list != undefined){
                    this.data = resp.data.list
                }
                this.total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }


    show(row: any) {
        console.log("show row = ", row);

        this.modal = true
        this.isadd = false
        this.formCustom = row
        this.formCustom.type = this.formCustom.type.toString()
    }


    add() {
        this.isadd = true
        this.modal = true
    }


    ok() {
        console.log("ok = ", this.formCustom)
        if(this.isadd){
            var obj = {...this.formCustom}
            obj.agent_id = (store.state as any).user.agent_id
            obj.type = parseInt(obj.type)
            ApplyTiXian(obj).then(resp => {
                if (resp.errcode == 0) {
                    this.$Message.success(resp.errmsg);
                    this.cancel()
                    this.getList()
                } else {
                    this.$Message.error(resp.errmsg)
                }
            });
        }else{
            var obj = {...this.formCustom}
            obj.agent_id = (store.state as any).user.agent_id
            obj.type = parseInt(obj.type)
            UpdateApplyTixian(obj).then(resp => {
                if (resp.errcode == 0) {
                    this.$Message.success(resp.errmsg);
                    this.cancel()
                    this.getList()
                } else {
                    this.$Message.error(resp.errmsg)
                }
            });
        }

    }

    cancel() {
        this.modal = false
    }

    page(page: number) {
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        this.pageValue.limit = limit
        this.getList()
    }

}
