import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { getRebateList } from '@/api/rebate';
import store from '@/store';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class WithdrawController extends Vue {

    modal: boolean = false
    modal1: boolean = false
    modal2: boolean = false

    columns: Array<TableHeader> = [
        {
            title: '订单号',
            slot: 'order_no',
            minWidth: 120
        },
        {
            title: '用户ID',
            slot: 'uid',
            minWidth: 80
        },
        {
            title: '佣金',
            slot: 'money',
            minWidth: 80
        },
        {
            title: '描述',
            slot: 'description',
            minWidth: 50
        },
        {
            title: '支付金额',
            slot: 'payment',
            minWidth: 50
        },
        {
            title: '创建时间',
            slot: 'created_at',
            minWidth: 80
        },
    ];

    total: number = 0
    data: Array<any> = []

    searchValue: any = {
        order_no:"",
        uid:"",
        created_at_start:"",
        created_at_end:"",
    }

    isadd: boolean = false

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }


    created() {
        this.getList()
    }

    getList() {
        console.log("searchValue = ", this.searchValue)
        let obj = { ...this.searchValue };
        if (obj.uid != "") {
            obj.uid = parseInt(obj.uid)
        }else{
            obj.uid = 0
        }
        obj.agent_id = (store.state as any).user.agent_id
        var param = { ...this.pageValue, ... obj };
        getRebateList(param).then(resp => {
            if (resp.errcode == 0) {
                this.data = resp.data.list
                this.total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }

    handleCreateTimeChange(time: any) {
        console.log("handleCreateTimeChange =", time)
        this.searchValue.created_at_start = time[0];
        this.searchValue.created_at_end = time[1];
    }

    search(){
        this.getList()
    }

    reset(){
        this.searchValue = {
            order_no:"",
            uid:"",
            created_at_start:"",
            created_at_end:"",
        }
        this.getList()
    }

    show(id: number) {
        this.modal1 = true
    }


    add() {
        this.isadd = true
        this.modal = true
    }


    ok() {
    }

    cancel() {
        this.modal = false
    }

    page(page: number) {
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        this.pageValue.limit = limit
        this.getList()
    }

}
