import * as api from "devapi";
import router from '@/router/router';
import { Message } from 'view-design';

export function InitGlobal(){
    if (process.env.VUE_APP_DOMAIN != undefined) {
        const DOMAIN: string = process.env.VUE_APP_DOMAIN
        api.GlobalUnit.BASE_URL = DOMAIN
    }

    api.GlobalUnit.BeginLoading = function () {
        console.log("BeginLoading")
    }
    api.GlobalUnit.EndLoading = function () {
        console.log("EndLoading")
    }
    api.GlobalUnit.HttpFailed = function (ok: boolean, status: number, statusText: string) {
        console.log("HttpFailed");
        (Message as any).error(statusText);
    }
    api.GlobalUnit.HttpErr = function (errCode: number, errMsg: string) {
        console.log("HttpErr");
        (Message as any).error(errMsg);
    }

}

export function SetJwt(){
    let jwt = localStorage.getItem('jwt')
    if (jwt != null) {
        api.GlobalUnit.JWT_STR = jwt
    }else{

        // 清除页签
        setTimeout(() => {
            router.push('/login');
        }, 200);
    }
}
