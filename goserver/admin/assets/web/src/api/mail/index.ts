import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import { GETMAILLIST, SENDMAIL } from "@/api/mail/uri";
const httpInstance = new HttpService(baseUrl);

export function getMailList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETMAILLIST, param);
}

export function sendMail(param: object): Promise<AjaxResponse> {
    return httpInstance.post(SENDMAIL, param);
}


