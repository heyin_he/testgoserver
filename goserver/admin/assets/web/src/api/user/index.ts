import { HttpService } from '../util';
import { baseUrl } from '../server';
import { GETUSERPERMISSION } from './uri';
import { AjaxResponse } from '@/type';
const httpInstance = new HttpService(baseUrl);

export function getUserPermission(param: { jwt : string}): Promise<AjaxResponse> {
    return httpInstance.get(GETUSERPERMISSION, param);
}
