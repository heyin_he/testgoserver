let baseUrl = "";
let wsUrl = "";
if (process.env.VUE_APP_DOMAIN != undefined) {
    const DOMAIN: string = process.env.VUE_APP_DOMAIN
    baseUrl = DOMAIN
}
export { baseUrl, wsUrl };