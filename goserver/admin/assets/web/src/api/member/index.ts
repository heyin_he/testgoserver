import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import {GETUSERLIST, GETAGENTLIST, CHECKUSERNAME, ADDAGENT, UPDATEAGENT, DELAGENT, FREEZE, FREEZEAGENT, UNFREEZE, UNFREEZEAGENT, GETAGENTINFO, GETAGENTCHILDLIST, SAVEBILI, GETBILI} from "@/api/member/uri";
const httpInstance = new HttpService(baseUrl);

export function getUserList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETUSERLIST, param);
}

export function getAgentList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETAGENTLIST, param);
}

export function checkUserName(param: object): Promise<AjaxResponse> {
    return httpInstance.post(CHECKUSERNAME, param);
}

export function addAgent(param: object): Promise<AjaxResponse> {
    return httpInstance.post(ADDAGENT, param);
}

export function updateAgent(param: object): Promise<AjaxResponse> {
    return httpInstance.post(UPDATEAGENT, param);
}


export function delAgent(param: object): Promise<AjaxResponse> {
    return httpInstance.post(DELAGENT, param);
}

export function Freeze(param: object): Promise<AjaxResponse> {
    return httpInstance.post(FREEZE, param);
}

export function UnFreeze(param: object): Promise<AjaxResponse> {
    return httpInstance.post(UNFREEZE, param);
}

export function getAgentInfo(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETAGENTINFO, param);
}

export function getAgentChildList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETAGENTCHILDLIST, param);
}


export function FreezeAgent(param: object): Promise<AjaxResponse> {
    return httpInstance.post(FREEZEAGENT, param);
}

export function UnFreezeAgent(param: object): Promise<AjaxResponse> {
    return httpInstance.post(UNFREEZEAGENT, param);
}

export function SaveBili(param:object): Promise<AjaxResponse> {
    return httpInstance.post(SAVEBILI, param);
}

export function GetBili(param:object): Promise<AjaxResponse> {
    return httpInstance.post(GETBILI, param);
}
