import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import { GETRECHARGELIST } from "@/api/recharge/uri";
const httpInstance = new HttpService(baseUrl);

export function getRechargeList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETRECHARGELIST, param);
}
