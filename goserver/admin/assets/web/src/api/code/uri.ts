export const ADDCODE = '/api/addcode';
export const UPDATECODE = '/api/updatecode';
export const DELCODE = '/api/delcode';
export const GETCODELIST = '/api/getcodelist';
export const CHANGESTATUS = '/api/changestatus';
