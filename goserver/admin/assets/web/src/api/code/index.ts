import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import { ADDCODE, UPDATECODE, DELCODE, GETCODELIST, CHANGESTATUS } from "@/api/code/uri";
const httpInstance = new HttpService(baseUrl);

export function getCodeList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETCODELIST, param);
}

export function addCode(param: object): Promise<AjaxResponse> {
    return httpInstance.post(ADDCODE, param);
}

export function updateCode(param: object): Promise<AjaxResponse> {
    return httpInstance.post(UPDATECODE, param);
}

export function delCode(param: object): Promise<AjaxResponse> {
    return httpInstance.post(DELCODE, param);
}

export function changeStatus(param: object): Promise<AjaxResponse> {
    return httpInstance.post(CHANGESTATUS, param);
}

