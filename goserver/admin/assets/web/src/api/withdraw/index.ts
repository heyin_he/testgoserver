import { HttpService } from '../util';
import { baseUrl } from '../server';
import { GETMYWITHDRAWLIST, DOWITHDRAW, DOWITHDRAWSTATUS, } from './uri';
import { AjaxResponse } from '@/type';
const httpInstance = new HttpService(baseUrl);

export function getMyWithdrawList(param: any): Promise<AjaxResponse> {
    return httpInstance.post(GETMYWITHDRAWLIST, param);
}

export function doWithdraw(param: any): Promise<AjaxResponse> {
    return httpInstance.post(DOWITHDRAW, param);
}

export function doWithdrawStatus(param: any): Promise<AjaxResponse> {
    return httpInstance.post(DOWITHDRAWSTATUS, param);
}