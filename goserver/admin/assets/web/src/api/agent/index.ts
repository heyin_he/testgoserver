import { HttpService } from '../util';
import { baseUrl } from '../server';
import { AjaxResponse } from '@/type';
import {AGENTLOGIN, GETSHAREURL, GETMYTIXIANLIST, APPLYTIXIAN, AGREETIXIAN} from "@/api/agent/uri";
const httpInstance = new HttpService(baseUrl);

export function login(param: object): Promise<AjaxResponse> {
    return httpInstance.post(AGENTLOGIN, param);
}


export function getShareUrl(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETSHAREURL, param);
}

export function GetMyTiXianList(param: object): Promise<AjaxResponse> {
    return httpInstance.post(GETMYTIXIANLIST, param);
}


export function ApplyTiXian(param: object): Promise<AjaxResponse> {
    return httpInstance.post(APPLYTIXIAN, param);
}

export function AgreeTiXian(param : object) : Promise<AjaxResponse>{
    return httpInstance.post(AGREETIXIAN, param);
}