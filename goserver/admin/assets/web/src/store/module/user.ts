import { User } from '@/type';
import * as types from '../mutation-types';
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import router, { resetRouter } from '@/router/router';
import storage from '@/assets/script/storage';
import { setLsCache, getLsCache } from '@/assets/script/util';
import cfg from '@/config';

type State = User & {};
const state: State = {
    username: getLsCache('username') || '',
    avatar: localStorage.getItem('avatar') || '',
    jwt: localStorage.getItem('jwt') || '',
    id: localStorage.getItem('id') || '',
    role_code: localStorage.getItem('role_code') || '',
    role_id: "",
    agent_id: localStorage.getItem('agent_id') || ""
};

export default {
    state,
    getters: {
        username: (_s: State) => _s.username,
        avatar: (_s: State) => _s.avatar,
        jwt: (_s: State) => _s.jwt,
        role_code: (_s: State) => _s.role_code,
        role_id: (_s: State) => _s.role_id,
        agent_id: (_s: State) => _s.agent_id,
    },
    mutations: {
        [types.UPDATE_USERNAME](_s: State, str: string): void {
            _s.username = str;
            if (str) {
                setLsCache({ key: 'username', val: str, ttl: cfg.userExpireTime });
            } else {
                localStorage.setItem('username', '');
            }
        },
        [types.UPDATE_AVATAR](_s: State, str: string): void {
            _s.avatar = str;
            localStorage.setItem('avatar', str);
        },
        [types.UPDATE_JWT](_s: State, str: string): void {
            _s.jwt = str;
            localStorage.setItem('jwt', str);
        },
        [types.UPDATE_ADMIN_ID](_s: State, str: string): void {
            _s.id = str;
            localStorage.setItem('id', str);
        },
        [types.UPDATE_ROLE_CODE](_s: State, str: string): void {
            _s.role_code = str;
            localStorage.setItem('role_code', str);
        },
        [types.UPDATE_ROLE_ID](_s: State, str: string): void {
            _s.role_id = str;
        },
        [types.UPDATE_AGENT_ID](_s: State, str: string): void {
            _s.agent_id = str;
            localStorage.setItem('agent_id', str);
        },
    },
    actions: {
        [types.UPDATE_USER]({ commit }: any, userinfo: User) {
            commit(types.UPDATE_ADMIN_ID, userinfo.id);
            commit(types.UPDATE_USERNAME, userinfo.username);
            commit(types.UPDATE_AVATAR, userinfo.avatar);
            commit(types.UPDATE_JWT, userinfo.jwt);
            commit(types.UPDATE_ROLE_CODE, userinfo.role_code);
            commit(types.UPDATE_ROLE_ID, userinfo.role_id);
            commit(types.UPDATE_AGENT_ID, userinfo.agent_id);
        },

        [types.UPDATE_ROLE]({ commit }: any, role_id: string) {
            commit(types.UPDATE_ROLE_ID, role_id);
        },

        [types.LOGOUT]({ dispatch, commit }: any) {
            // 清除用户信息 localstorage 和 store
            dispatch(types.UPDATE_USER, {
                id: '',
                username: '',
                avatar: '',
                jwt: '',
                role_code:"",
                role_id:'',
                agent_id:''
            });

            commit(types.CLEAR_VISITED_VIEW);
            // reset 有点问题
            window.location.reload();

            // 清除页签
            setTimeout(() => {
                router.push('/login');
            }, 200);
        }
    }
};
