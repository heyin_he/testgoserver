import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { getServerList, addServer, updateServer, getServerItemList, openServer } from '@/api/myserver';
import { getActivityList } from '@/api/activity';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class ServerController extends Vue {

    modal: boolean = false
    modal1: boolean = false
    modal2: boolean = false

    columns: Array<TableHeader> = [
        {
            title: '大区ID',
            slot: 'id',
            minWidth: 80
        },
        {
            title: '大区名称',
            slot: 'server_name',
            minWidth: 50
        },
        {
            title: '注册用户',
            slot: 'user_count',
            minWidth: 50
        },
        {
            title: '在线用户',
            slot: 'online_count',
            minWidth: 50
        },
        {
            title: '操作',
            slot: 'action',
            width: 150,
            align: 'center'
        }
    ];


    server_columns: Array<TableHeader> = [
        {
            title: '服ID',
            slot: 'id',
            minWidth: 80
        },
        {
            title: '活动名称',
            slot: 'activity_name',
            minWidth: 50
        },
        {
            title: '结束时间',
            slot: 'activity_end_time',
            minWidth: 80
        },
        {
            title: '注册用户',
            slot: 'user_count',
            minWidth: 50
        },
        {
            title: '在线用户',
            slot: 'online_count',
            minWidth: 50
        },
        {
            title: '状态',
            slot: 'status',
            minWidth: 100,
            filters: [
                {
                    label: '爆满',
                    value: 1
                },
                {
                    label: '新服',
                    value: 0
                }
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 1) {
                    return row.status === 1;
                } else if (value === 0) {
                    return row.status === 0;
                }
            }
        },
        {
            title: '开服时间',
            slot: 'created_at',
            minWidth: 120
        },
    ];

    total: number = 0
    server_total : number = 0
    data: Array<any> = []

    formCustom: any = {
        server_name : "",
    }

    serverCustom: any = {
        server_id: 0,
        activity_id:0,
        activity_end_time:"",
        server_num: 0,
    }

    server_data: Array<any> = []

    isadd: boolean = false

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }

    serverPageValue: NumberObj = {
        page: 1,
        limit: 100,
    }

    choose_id: number = 0

    searchValue: any = {
        start_time:"",
        end_time:"",
    }

    activity_data: Array<any> = [];

    created() {
        this.getList()
    }

    getList() {
        let obj = { ...this.searchValue };
        var param = { ...this.pageValue, ... obj };
        getServerList(param).then(resp => {
            if (resp.errcode == 0) {
                this.data = resp.data.list
                this.total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });

        getActivityList({}).then(resp => {
            if (resp.errcode == 0) {
                console.log("rlist = ",  resp.data.list)
                this.activity_data = resp.data.list
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }

    getActivityName(aid:number){
        for (let index = 0; index < this.activity_data.length; index++) {
            if (this.activity_data[index]._id == aid) {
                return this.activity_data[index].name
            }
        }
        return "-"
    }
    show(id: number) {
        this.modal1 = true
        this.choose_id = id
        let obj = { ...this.searchValue };
        var param = { ...this.serverPageValue, ...{ server_id : this.choose_id }, ... obj  };
        getServerItemList(param).then(resp => {
            if (resp.errcode == 0) {
                for (let index = 0; index < resp.data.list.length; index++) {
                    resp.data.list[index].activity_name = this.getActivityName(resp.data.list[index].activity_id)
                }
                this.server_data = resp.data.list
                this.server_total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }


    add() {
        this.isadd = true
        this.modal = true
    }


    ok() {
        if (this.isadd){
            addServer(this.formCustom).then(resp => {
                if (resp.errcode == 0) {
                    this.$Message.success(resp.errmsg);
                    this.cancel()
                    this.getList()
                } else {
                    this.$Message.error(resp.errmsg)
                }
            });
        }else{
            updateServer(this.formCustom).then(resp => {
                if (resp.errcode == 0) {
                    this.$Message.success(resp.errmsg);
                    this.cancel()
                    this.getList()
                } else {
                    this.$Message.error(resp.errmsg)
                }
            });
        }

    }

    cancel() {
        this.modal = false
        this.formCustom = {
            server_name : "",
        }
    }

    page(page: number) {
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        this.pageValue.limit = limit
        this.getList()
    }

    serverPage(page: number) {
        this.serverPageValue.page = page
        this.show(this.choose_id)
    }

    serverPageSize(limit: number) {
        this.serverPageValue.limit = limit
        this.show(this.choose_id)
    }

    formatStartTime(val: any){
        this.formCustom.start_expire = val;
        const date = new Date(this.formCustom.start_expire)
        var year = date.getFullYear()
        var month = date.getMonth() + 1
        var day = date.getDate()
        this.formCustom.start_expire = `${year}-${month}-${day}`
    }

    formatEndTime(val: any){
        this.formCustom.end_expire = val;
        const date = new Date(this.formCustom.end_expire)
        var year = date.getFullYear()
        var month = date.getMonth() + 1
        var day = date.getDate()
        this.formCustom.end_expire = `${year}-${month}-${day}`
    }

    set(row: any){
        this.modal2 = true
        console.log("set row = ", row)
        this.serverCustom.server_id = row.id
    }

    set_ok(){
        this.serverCustom.server_num = parseInt(this.serverCustom.server_num)

        openServer(this.serverCustom).then(resp => {
            if (resp.errcode == 0) {
                this.$Message.success(resp.errmsg);
                this.set_cancel()
                this.getList()
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }

    set_cancel(){
        this.modal2 = false
        this.serverCustom = {
            server_id: 0,
            server_num: 0,
        }
    }

    handleCreateTimeChange(time: any) {
        console.log("handleCreateTimeChange =", time)
        this.searchValue.start_time = time[0];
        this.searchValue.end_time = time[1];
    }

    handleActivityTimeChange(time: any) {
        console.log("handleActivityTimeChange =", time)
        this.serverCustom.activity_end_time = time
    }

    search(){
        this.getList()
    }
}
