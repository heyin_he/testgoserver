import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import * as api from "devapi";
import { Message } from 'view-design';
interface Obj {
    [key: string]: string | boolean | number;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class MenuController extends Vue {

    modal1: boolean = false

    columns: Array<TableHeader> = [
        {
            title: '菜单名称',
            key: 'name',
            minWidth: 100,
            tree: true
        },
        {
            title: '菜单标题',
            key: 'title',
            minWidth: 100
        },
        {
            title: '图标',
            key: 'icon',
            minWidth: 80
        },
        {
            title: '组件',
            key: 'component',
            minWidth: 120
        },
        {
            title: '路径',
            key: 'path',
            minWidth: 120
        },
        {
            title: '排序',
            key: 'sort',
            minWidth: 60
        },
        {
            title: '操作',
            slot: 'action',
            width: 150,
            align: 'center'
        }
    ];

    total: number = 0

    data: Array<any> = []

    formCustom: Obj = {
        id: "",
        parent_id: "",
        name: '',
        title: '',
        path: '',
        menu_type: "0",
        component: "",
        redirect: "",
        icon: "",
        sort: 0,
        perms: "",
        perms_type: "0",
        btn_status: "0",
        hideInMenu: false,
        hideInBread: false,
        notCache: false,
        isMmin: false,
    }


    ruleValidate: object = {

    }

    isadd: boolean = false

    searchValue: Obj = {
        role_name: '',
        role_code: '',
    }

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }

    created() {
        console.log("routes-----=-=-=-==-=--=->")
        this.getList()
    }

    getList() {
        api.Rbac.GetRbacGetroutelist((resp: api.GetRbacGetroutelistResponse) => {
            console.log("routes---->")

            if (resp.data != undefined && resp.data.routes != undefined) {
                console.log("routes---->", resp.data.routes)
                this.data = resp.data.routes
            }
        })
    }

    show(row: Obj) {
        this.isadd = false
        this.modal1 = true
        this.formCustom = row
        this.formCustom.menu_type = this.formCustom.menu_type.toString()
        this.formCustom.hideInMenu = this.formCustom.hideInMenu == "1"
        this.formCustom.hideInBread = this.formCustom.hideInBread == "1"
        this.formCustom.notCache = this.formCustom.notCache == "1"
        this.formCustom.isMmin = this.formCustom.isMmin == "1"
        console.log("show formCustom->", this.formCustom)
    }

    add() {
        this.isadd = true
        this.modal1 = true
        this.formCustom = {
            id: "",
            parent_id: "",
            name: '',
            title: '',
            path: '',
            menu_type: "0",
            component: "",
            redirect: "",
            icon: "",
            sort: 0,
            perms: "",
            perms_type: "0",
            btn_status: "0",
            hideInMenu: false,
            hideInBread: false,
            notCache: false,
            isMmin: false,
        }
    }

    remove(id: string) {
        return () => {
            api.Rbac.GetRbacDelroute(id, (resp: api.GetRbacDelrouteResponse) => {
                this.$Message.success(resp.errmsg);
                this.getList();
            })
        }
    }
    ok() {
        console.log('测试路由');
        let btn_status: number = parseInt(this.formCustom.btn_status.toString())
        let hideInBread: number = this.formCustom.hideInBread ? 1 : 0
        let hideInMenu: number = this.formCustom.hideInMenu ? 1 : 0
        let isMmin: number = this.formCustom.isMmin ? 1 : 0
        let notCache: number = this.formCustom.notCache ? 1 : 0
        let perms_type: number = this.formCustom.perms_type ? 1 : 0
        if (this.isadd) {
            api.Rbac.PostRbacCreateroute(btn_status,
                this.formCustom.component.toString(),
                hideInBread,
                hideInMenu,
                this.formCustom.icon.toString(),
                isMmin,
                parseInt(this.formCustom.menu_type.toString()),
                this.formCustom.name.toString(),
                notCache,
                this.formCustom.parent_id.toString(),
                this.formCustom.path.toString(),
                this.formCustom.perms.toString(),
                perms_type,
                this.formCustom.redirect.toString(),
                parseInt(this.formCustom.sort.toString()),
                this.formCustom.title.toString(),
                (resp: api.PostRbacCreaterouteResponse) => {
                    if ( resp.errcode == 0 ) {
                        this.$Message.success(resp.errmsg);
                    this.modal1 = false
                    this.getList();
                    } else {
                        this.$Message.error(resp.errmsg)
                    }
                })
        } else {
            api.Rbac.PostRbacUpdateroute(
                btn_status,
                this.formCustom.component.toString(),
                hideInBread,
                hideInMenu,
                this.formCustom.icon.toString(),
                isMmin,
                parseInt(this.formCustom.menu_type.toString()),
                this.formCustom.name.toString(),
                notCache,
                this.formCustom.parent_id.toString(),
                this.formCustom.path.toString(),
                this.formCustom.perms.toString(),
                perms_type,
                this.formCustom.redirect.toString(),
                this.formCustom.id.toString(),
                parseInt(this.formCustom.sort.toString()),
                this.formCustom.title.toString(),
                (resp: api.PostRbacUpdaterouteResponse) => {
                    if ( resp.errcode == 0 ) {
                        this.$Message.success(resp.errmsg);
                    this.modal1 = false
                    this.getList();
                    } else {
                        this.$Message.error(resp.errmsg)
                    }
                })
        }
    }

    cancel() {
        this.isadd = false
        this.modal1 = false
        this.formCustom = {
            id: "",
            parent_id: "",
            name: '',
            title: '',
            path: '',
            menu_type: "0",
            component: "",
            redirect: "",
            icon: "",
            sort: 0,
            perms: "",
            perms_type: "0",
            btn_status: "0",
            hideInMenu: false,
            hideInBread: false,
            notCache: false,
            isMmin: false,
        }
    }

    changehideInMenu(status: boolean) {
        this.formCustom.hideInMenu = status
    }

    changehideInBread(status: boolean) {
        this.formCustom.hideInBread = status
    }


    changenotCache(status: boolean) {
        this.formCustom.notCache = status
    }

    changeisMmin(status: boolean) {
        this.formCustom.isMmin = status
    }

    page(page: number) {
        console.log("分页--->", page)
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        console.log("分页 size---->", limit)
        this.pageValue.limit = limit
        this.getList()
    }
}