import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { getActivityList, updateActivity } from '@/api/activity';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class ActivityController extends Vue {

    modal: boolean = false

    columns: Array<TableHeader> = [
        {
            title: '活动名称',
            slot: 'name',
            minWidth: 80
        },
        // {
        //     title: '状态',
        //     slot: 'status',
        //     minWidth: 100,
        //     filters: [
        //         {
        //             label: '开启',
        //             value: 1
        //         },
        //         {
        //             label: '关闭',
        //             value: 0
        //         }
        //     ],
        //     filterMultiple: false,
        //     filterMethod(value: number, row: any) {
        //         if (value === 0) {
        //             return row.status === 0;
        //         } else if (value === 1) {
        //             return row.status === 1;
        //         }
        //     }
        // },
        {
            title: '开始时间',
            slot: 'start_time',
            minWidth: 100
        },
        {
            title: '结束时间',
            slot: 'end_time',
            minWidth: 100
        },
        {
            title: '操作',
            slot: 'action',
            width: 150,
            align: 'center'
        }
    ];

    total: number = 0
    data: Array<any> = []

    formCustom: any = {
        name: '',
        status: 0,
        start_time: '',
        end_time: '',
    }

    isadd: boolean = false

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }

    created() {
        this.getList()
    }

    getList() {
        getActivityList(this.pageValue).then(resp => {
            if (resp.errcode == 0) {
                this.data = resp.data.list
                this.total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }


    show(row: any) {
        console.log("show row = ", row)
        this.isadd = false
        this.modal = true
        row.status = row.status.toString()
        this.formCustom = row
    }


    add() {
        this.isadd = true
        this.modal = true
    }

    remove(row: any) {
        return () => {
        }
    }

    changeStatus(row: any, status: number){
        return () => {
        }
    }


    ok() {
        this.formCustom.status = parseInt(this.formCustom.status)
        updateActivity(this.formCustom).then(resp => {
            if (resp.errcode == 0) {
                this.$Message.success(resp.errmsg);
                this.cancel()
                this.getList()
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }

    cancel() {
        this.modal = false
        this.formCustom = {
            name: '',
            status: 0,
            start_time: '',
            end_time: '',
        }
    }

    page(page: number) {
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        this.pageValue.limit = limit
        this.getList()
    }

    handleTimeChange(time: any) {
        console.log("handleTimeChange =", time)
        this.formCustom.start_time = time[0];
        this.formCustom.end_time = time[1];
    }
}