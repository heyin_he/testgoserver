import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { getAgentList, checkUserName, addAgent, updateAgent, delAgent, FreezeAgent, UnFreezeAgent, SaveBili, GetBili } from '@/api/member';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class MemberController extends Vue {

    modal: boolean = false

    isadd :boolean = false

    columns: Array<TableHeader> = [
        {
            title: '用户ID',
            slot: '_id',
            minWidth: 80
        },
        {
            title: '账号',
            slot: 'username',
            minWidth: 80
        },
        {
            title: '姓名',
            slot: 'realname',
            minWidth: 80
        },
        {
            title: '佣金',
            slot: 'money',
            minWidth: 80
        },
        {
            title: '收款方式',
            slot: 'type',
            minWidth: 80,
        },
        {
            title: '收款账号',
            slot: 'account',
            minWidth: 80
        },
        {
            title: '账号状态',
            slot: 'disable',
            minWidth: 50,
            filters: [
                {
                    label: '封号',
                    value: 1
                },
                {
                    label: '正常',
                    value: 0
                }
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 1) {
                    return row.disable === 1;
                } else if (value === 0) {
                    return row.disable === 0;
                }
            }
        },

        {
            title: '创建时间',
            slot: 'created_at',
            minWidth: 120
        },
        {
            title: '操作',
            slot: 'action',
            width: 250,
            align: 'center'
        }
    ];

    RuleValidate: object = {
        username: [
            { required: true, message: '请输入账号', trigger: 'blur' },
            // { pattern: "/^(?=.*[a-zA-Z]+)(?=.*[0-9]+)[a-zA-Z0-9]{4,20}$/", message: '请输入4-20位数字和字母组成的账号', trigger: 'blur' }
            { validator: this.checkUserName, trigger: 'blur' }
        ],
        password: [
            { required: true, message: '请输入密码', trigger: 'blur' },
            { validator: this.checkPassword, trigger: 'blur' }
        ],
        secondpassword: [
            { required: true, message: '请再次输入密码', trigger: 'blur' },
            { validator: this.surePwd, trigger: 'blur' }
        ],
    }

    total: number = 0
    data: Array<any> = []

    formCustom: Obj = {
        username: '',
        password: '',
        secondpassword: "",
        realname: ""
    }

    searchValue: any = {
        id: '',
        username: '',
    }

    pageValue: NumberObj = {
        page: 1,
        limit: 20,
    }

    bili: number = 0.0

    created() {
        this.getList()
        this.getbili()
    }

    getList() {
        console.log("searchValue = ", this.searchValue)
        let obj = { ...this.searchValue };
        var param = { ...this.pageValue, ... obj };
        getAgentList(param).then(resp => {
            if (resp.errcode == 0) {
                this.data = resp.data.list
                this.total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }

    async getbili() {
        console.log("getbili")
        GetBili({}).then(resp => {
            if (resp.errcode == 0) {
                this.bili = resp.data.bili
            } else {
                this.$Message.error(resp.errmsg)
            }
        });

    }

    checkPassword(rule: any, value: string, callback: Function) {
        if (value == "") {
            callback(new Error("请输入密码!"))
        } else if (value != "") {
            var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/
            if (!regex.test(value)) {
                callback(new Error("必须包含大小写字母和数字的组合，可以使用特殊字符，长度在8-16之间"))
            } else {
                callback()
            }
        } else {
            callback()
        }
    }

    checkUserName(rule: any, value: string, callback: Function) {
        if (!this.isadd) {
            callback()
            return
        }
        if (value == "") {
            callback(new Error("请输入账号!"))
        } else if (value != "") {
            checkUserName({ "username": value }).then(resp => {
                if (resp.errcode > 0) {
                    callback(new Error(resp.errmsg))
                } else {
                    callback()
                }
            });
        } else {
            callback()
        }
    }

    surePwd(rule: any, value: string, callback: Function) {
        if (value == "") {
            callback(new Error("请再次输入密码!"))
        } else if (value != this.formCustom.password) {
            callback(new Error("两次输入密码不一致!"))
        } else {
            callback()
        }
    }

    search() {

        this.getList()
    }

    reset() {
        this.pageValue.page = 1
        this.pageValue.limit = 20
        this.getList()
    }

    page(page: number) {
        console.log("分页--->", page)
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        console.log("分页 size---->", limit)
        this.pageValue.limit = limit
        this.getList()
    }

    show(row: any) {
        console.log("show row = ", row)
        this.modal = true
        this.isadd = false
        this.formCustom = row
    }

    //取消代理
    del(row: any) {
        return () => {
            delAgent({ "uid": row._id }).then(resp => {
                if (resp.errcode > 0) {
                    this.$Message.error(resp.errmsg)
                    return
                } else {
                    this.$Message.success("删除代理成功")
                    this.cancel()
                    this.getList()
                }
            });
        }
    }

    ok() {
        console.log("formCustom = ", this.formCustom)
        if(this.isadd){
            addAgent(this.formCustom).then(resp => {
                if (resp.errcode > 0) {
                    this.$Message.error(resp.errmsg)
                    return
                } else {
                    this.$Message.success("代理添加成功")
                    this.cancel()
                    this.getList()
                }
            });
        }else{
            updateAgent(this.formCustom).then(resp => {
                if (resp.errcode > 0) {
                    this.$Message.error(resp.errmsg)
                    return
                } else {
                    this.$Message.success("代理修改成功")
                    this.cancel()
                    this.getList()
                }
            });
        }
    }

    cancel() {
        this.modal = false
        this.formCustom.uid = ""
        this.formCustom.username = ""
        this.formCustom.password = ""
        this.formCustom.secondpassword = ""
        this.formCustom.realname = ""
    }

    freeze(row: any) {
        return () => {
            console.log("freeze = ", row)
            FreezeAgent({ "agent_id": row._id }).then(resp => {
                if (resp.errcode > 0) {
                    this.$Message.error(resp.errmsg)
                    return
                } else {
                    this.$Message.success("冻结成功")
                    this.cancel()
                    this.getList()
                }
            });
        }
    }

    unfreeze(row: any) {
        return () => {
            console.log("unfreeze = ", row)
            UnFreezeAgent({ "agent_id": row._id}).then(resp => {
                if (resp.errcode > 0) {
                    this.$Message.error(resp.errmsg)
                    return
                } else {
                    this.$Message.success("解冻成功")
                    this.cancel()
                    this.getList()
                }
            });
        }
    }

    add(){
        console.log("add")
        this.modal = true
        this.isadd = true
    }

    save(){
        console.log("bili = ", this.bili)
        SaveBili({ "bili": parseFloat(this.bili.toString()) }).then(resp => {
            if (resp.errcode > 0) {
                this.$Message.error(resp.errmsg)
                return
            } else {
                this.$Message.success("保存成功")
            }
        });
    }
}
