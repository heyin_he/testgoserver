import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { DatePicker, Input } from 'view-design';
import { getUserList, checkUserName, addAgent, delAgent, Freeze, UnFreeze } from '@/api/member';
import moment from 'moment';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class MemberController extends Vue {

    modal: boolean = false

    columns: Array<TableHeader> = [
        {
            title: '用户ID',
            slot: 'uid',
            minWidth: 80
        },
        {
            title: '昵称',
            slot: 'nickname',
            minWidth: 80
        },
        {
            title: '当前关卡',
            slot: 'max_level',
            minWidth: 80
        },
        {
            title: '区ID',
            slot: 'regionid',
            minWidth: 80
        },
        {
            title: '服ID',
            slot: 'serverid',
            minWidth: 80
        },
        {
            title: '邀请人数',
            slot: 'referrer_count',
            minWidth: 80
        },
        {
            title: '在线',
            slot: 'online_status',
            minWidth: 50,
            filters: [
                {
                    label: '在线',
                    value: 1
                },
                {
                    label: '离线',
                    value: 0
                }
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 1) {
                    return row.online_status === 1;
                } else if (value === 0) {
                    return row.online_status === 0;
                }
            }
        },
        {
            title: '状态',
            slot: 'account_status',
            minWidth: 50,
            filters: [
                {
                    label: '封号',
                    value: 1
                },
                {
                    label: '正常',
                    value: 0
                }
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 1) {
                    return row.account_status === 1;
                } else if (value === 0) {
                    return row.account_status === 0;
                }
            }
        },
        {
            title: '封号原因',
            slot: 'freeze_reason',
            minWidth: 120
        },
        {
            title: '封号截止日期',
            slot: 'freeze_date',
            minWidth: 120
        },
        {
            title: '最后登录时间',
            slot: 'last_login_time',
            minWidth: 120
        },
        {
            title: '最后离线时间',
            slot: 'last_logout_time',
            minWidth: 120
        },
        {
            title: '创建时间',
            slot: 'created_at',
            minWidth: 120
        },
        // {
        //     title: '操作',
        //     slot: 'action',
        //     width: 100,
        //     align: 'center'
        // }
    ];

    RuleValidate: object = {
        username: [
            { required: true, message: '请输入账号', trigger: 'blur' },
            // { pattern: "/^(?=.*[a-zA-Z]+)(?=.*[0-9]+)[a-zA-Z0-9]{4,20}$/", message: '请输入4-20位数字和字母组成的账号', trigger: 'blur' }
            { validator: this.checkUserName, trigger: 'blur' }
        ],
        password: [
            { required: true, message: '请输入密码', trigger: 'blur' },
            { validator: this.checkPassword, trigger: 'blur' }
        ],
        secondpassword: [
            { required: true, message: '请再次输入密码', trigger: 'blur' },
            { validator: this.surePwd, trigger: 'blur' }
        ],
    }

    total: number = 0
    data: Array<any> = []

    formCustom: Obj = {
        uid: '',
        username: '',
        password: '',
        secondpassword: "",
        realname: ""
    }

    searchValue: any = {
        uid: '',
    }

    pageValue: NumberObj = {
        page: 1,
        limit: 20,
    }

    freezeReason: string = ''
    freezeDay: string = ''

    valueFormat: any = {
        default: 'yyyy-MM-dd HH:mm:ss',
        type: String
    }

    created() {
        this.getList()
    }

    getList() {
        console.log("searchValue = ", this.searchValue)
        let obj = { ...this.searchValue };
        if (obj.uid != "") {
            obj.uid = parseInt(obj.uid)
        } else {
            obj.uid = 0
        }
        var param = { ...this.pageValue, ...obj };
        getUserList(param).then(resp => {
            if (resp.errcode == 0) {
                for (let index = 0; index < resp.data.list.length; index++) {
                    if (resp.data.list[index].account_status == 1) {
                        if (resp.data.list[index].freeze_date != ""){
                            const timestamp = new Date(resp.data.list[index].freeze_date).getTime();
                            const now = new Date().getTime();
                            if(now >timestamp){
                                resp.data.list[index].account_status = 0
                            }
                        }
                    }
                    resp.data.list[index].referrer_count = resp.data.referrer[resp.data.list[index].uid]
                }
                this.data = resp.data.list
                this.total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }

    checkPassword(rule: any, value: string, callback: Function) {
        if (value == "") {
            callback(new Error("请输入密码!"))
        } else if (value != "") {
            var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/
            if (!regex.test(value)) {
                callback(new Error("必须包含大小写字母和数字的组合，可以使用特殊字符，长度在8-16之间"))
            } else {
                callback()
            }
        } else {
            callback()
        }
    }

    checkUserName(rule: any, value: string, callback: Function) {
        if (value == "") {
            callback(new Error("请输入账号!"))
        } else if (value != "") {
            checkUserName({ "username": value }).then(resp => {
                if (resp.errcode > 0) {
                    callback(new Error(resp.errmsg))
                } else {
                    callback()
                }
            });
        } else {
            callback()
        }
    }

    surePwd(rule: any, value: string, callback: Function) {
        if (value == "") {
            callback(new Error("请再次输入密码!"))
        } else if (value != this.formCustom.password) {
            callback(new Error("两次输入密码不一致!"))
        } else {
            callback()
        }
    }

    search() {

        this.getList()
    }

    reset() {
        this.pageValue.page = 1
        this.pageValue.limit = 20
        this.getList()
    }

    page(page: number) {
        console.log("分页--->", page)
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        console.log("分页 size---->", limit)
        this.pageValue.limit = limit
        this.getList()
    }

    show(row: any) {
        console.log("show row = ", row)
        this.modal = true
        this.formCustom.uid = row.uid
        this.formCustom.realname = row.nickname
    }

    //取消代理
    del(row: any) {
        console.log("del row = ", row)
        return () => {
            delAgent({ "uid": row.uid }).then(resp => {
                if (resp.errcode > 0) {
                    this.$Message.error(resp.errmsg)
                    return
                } else {
                    this.$Message.success("取消代理成功")
                    this.cancel()
                    this.getList()
                }
            });
        }
    }

    ok() {
        console.log("formCustom = ", this.formCustom)
        addAgent(this.formCustom).then(resp => {
            if (resp.errcode > 0) {
                this.$Message.error(resp.errmsg)
                return
            } else {
                this.$Message.success("代理添加成功")
                this.cancel()
                this.getList()
            }
        });
    }

    cancel() {
        this.modal = false
        this.formCustom.uid = ""
        this.formCustom.username = ""
        this.formCustom.password = ""
        this.formCustom.secondpassword = ""
        this.formCustom.realname = ""
    }


    freeze(row: any) {
        var that = this;
        return () => {
            const h = that.$createElement;
            that.freezeReason = ""
            that.$Modal.confirm({
                title: '请输入冻结原因',
                render: () => {
                    return h(Input, {
                        props: {
                            value: that.freezeReason,
                            placeholder: '请输入冻结原因'
                        },
                        on: {
                            input: (val: string) => {
                                that.freezeReason = val;
                            }
                        }
                    });
                },
                onOk: () => {
                    if (that.freezeReason == "") {
                        that.$Message.error('请输入冻结原因');
                        return;
                    }
                    setTimeout(() => {
                        that.$Modal.confirm({
                            title: '请选择结束时间',
                            render: () => {
                                return h(DatePicker, {
                                    props: {
                                        value: that.freezeDay,
                                        format: "yyyy-MM-dd HH:mm:ss",
                                        editable:false,
                                        placeholder: '请选择结束时间',
                                        type: "datetime"
                                    },
                                    on: {
                                        input: (val: string) => {
                                            var date = new Date(val);
                                            that.freezeDay = moment(date).format('YYYY-MM-DD HH:mm:ss');;
                                        }
                                    }
                                });
                            },
                            onOk: () => {
                                if (that.freezeDay == "") {
                                    that.$Message.error('请输入冻结时长');
                                    return;
                                }
                                Freeze({ "uid": row.uid, "freeze_reason": that.freezeReason, "freeze_date": that.freezeDay }).then(resp => {
                                    if (resp.errcode > 0) {
                                        that.$Message.error(resp.errmsg)
                                        return
                                    } else {
                                        that.$Message.success("冻结成功")
                                        that.cancel()
                                        that.getList()
                                    }
                                });
                            }
                        });
                    }, 500)
                }
            });

        }
    }


    unfreeze(row: any) {
        return () => {
            console.log("unfreeze = ", row)
            UnFreeze({ "uid": row.uid }).then(resp => {
                if (resp.errcode > 0) {
                    this.$Message.error(resp.errmsg)
                    return
                } else {
                    this.$Message.success("解冻成功")
                    this.cancel()
                    this.getList()
                }
            });
        }
    }
}

