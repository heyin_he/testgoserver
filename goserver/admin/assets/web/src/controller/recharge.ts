import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { getRechargeList } from '@/api/recharge';
import NumberCard from '../views/home/NumberCard.vue';
import MyChart from '../views/home/MyChart.vue';
import DynamicCard from '../views/home/DynamicCard.vue';

interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm, NumberCard, MyChart, DynamicCard }
})
export default class WithdrawController extends Vue {

    modal: boolean = false
    modal1: boolean = false
    modal2: boolean = false

    is_show: boolean = false

    Loading: boolean = false;

    columns: Array<TableHeader> = [
        {
            title: '订单号',
            slot: 'order_no',
            minWidth: 120
        },
        {
            title: '用户ID',
            slot: 'uid',
            minWidth: 80
        },
        {
            title: '区ID',
            slot: 'regionid',
            minWidth: 50
        },
        {
            title: '服ID',
            slot: 'serverid',
            minWidth: 50
        },
        {
            title: '描述',
            slot: 'description',
            minWidth: 80
        },
        {
            title: '支付金额',
            slot: 'payment',
            minWidth: 50
        },
        {
            title: '支付状态',
            slot: 'pay_status',
            minWidth: 50,
            filters: [
                {
                    label: '已支付',
                    value: 1
                },
                {
                    label: '待支付',
                    value: 0
                },
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 0) {
                    return row.pay_status === 0;
                } else if (value === 1) {
                    return row.pay_status === 1;
                }
            }
        },
        {
            title: '支付时间',
            slot: 'pay_at',
            minWidth: 80
        },
        {
            title: '完成状态',
            slot: 'complete',
            minWidth: 50,
            filters: [
                {
                    label: '已完成',
                    value: 1
                },
                {
                    label: '待完成',
                    value: 0
                },
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 0) {
                    return row.complete === 0;
                } else if (value === 1) {
                    return row.complete === 1;
                }
            }
        },

        {
            title: '完成时间',
            slot: 'completed_at',
            minWidth: 80
        },
        {
            title: '创建时间',
            slot: 'created_at',
            minWidth: 80
        },
    ];

    total: number = 0
    data: Array<any> = []

    searchValue: any = {
        order_no:"",
        uid:"",
        regionid:"",
        serverid:"",
        pay_at_start:"",
        pay_at_end:"",
        created_at_start:"",
        created_at_end:"",
    }

    isadd: boolean = false

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }


    cardData: Array<any> = [];

    dayChartType = {
        id: '2',
        targetName: '充值统计',
        chartStyle: 'line',
        chartName: 'income',
        unit: '(元)',
        respStatus: 'noData'
    };
    dayChartData : any = {
        columns: ['日期', '充值金额'],
        rows: [{ '日期': '1/1', '充值金额': 1393},
        { '日期': '1/2', '充值金额': 3530},
        { '日期': '1/3', '充值金额': 2923},
        { '日期': '1/4', '充值金额': 1723},
        { '日期': '1/5', '充值金额': 3792},
        { '日期': '1/6', '充值金额': 4593}]
    };

    chartSettings = {}

    created() {
        this.getList()
    }

    getList() {
        let obj = { ...this.searchValue };
        if (obj.uid != "") {
            obj.uid = parseInt(obj.uid)
        }else{
            obj.uid = 0
        }
        if (obj.serverid != "") {
            obj.serverid = parseInt(obj.serverid)
        }else{
            obj.serverid = 0
        }
        if (obj.regionid != "") {
            obj.regionid = parseInt(obj.regionid)
        }else{
            obj.regionid = 0
        }
        var param = { ...this.pageValue, ... obj };
        getRechargeList(param).then(resp => {
            if (resp.errcode == 0) {
                this.data = resp.data.list
                this.total = resp.data.total
                this.doChart()
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }


    doChart(){
        var total = 0
        for (let index = 0; index < this.data.length; index++) {
            total = total + this.data[index].payment;

        }
        this.cardData = [
            {
                startVal: 0,
                targetValue: total,
                targetName: '充值金额',
                targetCell: '元',
                explanation: '充值总额',
                icon: 'logo-bitcoin',
            },
        ];

        this.dayChartData.rows = [
              { '日期': '1/1', '充值金额': 1393},
              { '日期': '1/2', '充值金额': 3530},
              { '日期': '1/3', '充值金额': 2923},
              { '日期': '1/4', '充值金额': 1723},
              { '日期': '1/5', '充值金额': 3792},
              { '日期': '1/6', '充值金额': 4593}
            ]
        console.log("dayChartData = ", this.dayChartData)
    }

    handlePayTimeChange(time: any) {
        console.log("handlePayTimeChange =", time)
        this.searchValue.pay_at_start = time[0];
        this.searchValue.pay_at_end = time[1];
    }

    handleCreateTimeChange(time: any) {
        console.log("handleCreateTimeChange =", time)
        this.searchValue.created_at_start = time[0];
        this.searchValue.created_at_end = time[1];
    }

    search(){
        this.getList()
    }

    reset(){
        this.searchValue = {
            order_no:"",
            uid:"",
            regionid:"",
            serverid:"",
            pay_at_start:"",
            pay_at_end:"",
            created_at_start:"",
            created_at_end:"",
        }
        this.getList()
    }

    show(id: number) {
        this.modal1 = true
    }


    add() {
        this.isadd = true
        this.modal = true
    }


    ok() {
    }

    cancel() {
        this.modal = false
    }

    page(page: number) {
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        this.pageValue.limit = limit
        this.getList()
    }


    detail(){
        this.is_show = true
    }

    changeDayChartType(chartStyle: string, chartName: string) {
        this.dayChartType.chartStyle = chartStyle;
        this.dayChartType.chartName = chartName;
    }


}
