import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import * as api from "devapi";
import { Message, TreeChild } from 'view-design';
import { Tree } from 'iview';
import { resolve } from 'path/posix';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class RoleController extends Vue {
    split1: number = 1
    modal1: boolean = false
    isShowUser: boolean = false
    auth: boolean = false
    edit1: boolean = false
    has1: boolean = false
    columns: Array<TableHeader> = [
        {
            title: '角色名称',
            slot: 'role_name',
            minWidth: 100
        },
        {
            title: '角色编码',
            slot: 'role_code',
            minWidth: 80
        },
        {
            title: '描述',
            slot: 'description',
            minWidth: 180
        },
        {
            title: '时间',
            slot: 'created_at',
            minWidth: 120
        },
        {
            title: '操作',
            slot: 'action',
            width: 250,
            align: 'center'
        }
    ];

    usercolumns: Array<TableHeader> = [
        {
            title: '账号',
            slot: 'username',
            minWidth: 100
        },
        {
            title: '姓名',
            slot: 'realname',
            minWidth: 80
        },
        {
            title: '状态',
            slot: 'disable',
            minWidth: 100,
            filters: [
                {
                    label: '正常',
                    value: 0
                },
                {
                    label: '冻结',
                    value: 1
                }
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 1) {
                    return row.disable === 1;
                } else if (value === 0) {
                    return row.disable === 0;
                }
            }
        },
        {
            title: '操作',
            slot: 'action',
            width: 150,
            align: 'center'
        }
    ];


    allusercolumns: Array<TableHeader> = [
        {
            type: 'selection',
            width: 60,
            align: 'center'
        },
        {
            title: '账号',
            slot: 'username',
            minWidth: 100
        },
        {
            title: '姓名',
            slot: 'realname',
            minWidth: 80
        },
        {
            title: '状态',
            slot: 'disable',
            minWidth: 100,
            filters: [
                {
                    label: '正常',
                    value: 0
                },
                {
                    label: '冻结',
                    value: 1
                }
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 1) {
                    return row.disable === 1;
                } else if (value === 0) {
                    return row.disable === 0;
                }
            }
        }
    ];

    total: number = 0
    usertotal: number = 0
    alluserlisttotal: number = 0
    data: Array<api.RbacGetRoleList> = []
    userList: Array<api.RbacUserListDataUserList> = []

    alluserlist: Array<api.RbacUserListDataUserList> = []

    formCustom: Obj = {
        id: "",
        role_name: '',
        role_code: '',
        description: "",
    }


    userRuleValidate: object = {
        role_name: [
            { required: true, message: '请输入角色名称', trigger: 'blur' },
        ],
        role_code: [
            { required: true, message: '请输入角色编码', trigger: 'blur' },
            { validator: this.checkCode, trigger: 'blur' }
        ],

    }

    isadd: boolean = false

    isadduser: boolean = false

    searchValue: Obj = {
        role_name: '',
        role_code: '',
    }

    allUsersearchValue: Obj = {
        username: "",
    }

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }

    userPageValue: NumberObj = {
        page: 1,
        limit: 10,
    }

    allUserPageValue: NumberObj = {
        page: 1,
        limit: 10,
    }

    routes: Array<any> = []

    auth_id: string = ""

    show_user_role_id = ""

    userInfo: Obj = {
        id: "",
        username: '',
        realname: '',
        password: "",
        second_password: "",
        avatar: '',
        root: "0"
    }


    avatars: Array<Obj> = [
        {
            "avatar": api.GlobalUnit.BASE_URL + "assets/avatar.png",
        },
        {
            "avatar": api.GlobalUnit.BASE_URL + "assets/avatar1.jpeg",
        },
        {
            "avatar": api.GlobalUnit.BASE_URL + "assets/avatar2.jpeg",
        },
        {
            "avatar": api.GlobalUnit.BASE_URL + "assets/avatar3.jpeg",
        }
    ]

    uids: Array<string> = []

    created() {
        this.getList()
    }

    getList() {
        api.Rbac.PostRbacGetroles(this.pageValue.limit, this.pageValue.page, this.searchValue.realname, this.searchValue.username, (resp: api.PostRbacGetrolesResponse) => {
            if (resp.data != undefined && resp.data.roles != undefined && resp.data.count != undefined) {
                this.data = resp.data.roles
                this.total = resp.data.count
            }
        })
    }

    checkCode(role: string, value: string, callback: Function) {
        if (value == "") {
            callback(new Error("请输入角色名称!"))
        } else if (value != "") {
            api.Rbac.GetRbacCheckrolecode(value, (resp: api.GetRbacCheckrolecodeResponse) => {
                if(resp.errcode > 0){
                    callback(new Error(resp.errmsg))
                }else{
                    callback()
                }
            }, false, false)
        } else {
            callback()
        }
    }

    search() {
        this.getList()
    }

    reset() {
        this.searchValue = {
            role_code: '',
            role_name: '',
        }
        this.getList()
    }

    show(row: Obj) {
        this.isadd = false
        this.modal1 = true
        this.formCustom.id = row.id
        this.formCustom.role_code = row.role_code
        this.formCustom.role_name = row.role_name
        this.formCustom.description = row.description
    }

    toMakeTree(routes: Array<any>) {
        routes.forEach(item => {
            item.expand = true
            item.checked = item.checked == 1 ? true : false
            if (item.children != undefined && item.children.length > 0) {
                this.toMakeTree.call(this, item.children)
            }

        })
        return routes
    }

    authorize(id: string) {
        this.auth = true
        this.auth_id = id
        api.Rbac.GetRbacGetroutesbyroleid(id, (resp: api.GetRbacGetroutesbyroleidResponse) => {
            if (resp.data != undefined && resp.data.routes != undefined) {
                let list = this.toMakeTree(resp.data.routes)
                this.routes = list
            }
        })
    }

    cancelbind() {
        this.auth = false
        this.auth_id = ""
    }

    add() {
        this.isadd = true
        this.modal1 = true
        this.formCustom.role_code = ""
        this.formCustom.role_name = ""
        this.formCustom.description = ""
    }

    remove(id: string) {
        return () => {
            console.log("id------>", id)
            api.Rbac.GetRbacDelrole(id, (resp: api.GetRbacDelroleResponse) => {
                this.$Message.success(resp.errmsg);
                this.getList();
            })
        }
    }

    ok() {
        if (this.isadd) {
            api.Rbac.PostRbacCreaterole(this.formCustom.description, this.formCustom.role_code, this.formCustom.role_name, (resp: api.PostRbacCreateroleResponse) => {
                this.$Message.success(resp.errmsg);
                this.modal1 = false
                this.getList();

            })
        } else {
            api.Rbac.PostRbacUpdaterole(this.formCustom.description, this.formCustom.id, this.formCustom.role_name, (resp: api.PostRbacUpdateroleResponse) => {
                this.$Message.success(resp.errmsg);
                this.modal1 = false
            })
        }
    }

    cancel() {
        this.isadd = false
        this.modal1 = false
        this.formCustom.role_code = ""
        this.formCustom.role_name = ""
        this.formCustom.description = ""
    }

    page(page: number) {
        console.log("分页--->", page)
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        console.log("分页 size---->", limit)
        this.pageValue.limit = limit
        this.getList()
    }

    allUserPage(page: number) {
        console.log("分页--->", page)
        this.allUserPageValue.page = page
        this.addHasUser()
    }

    allUserPageSize(limit: number) {
        console.log("分页 size---->", limit)
        this.allUserPageValue.limit = limit
        this.addHasUser()
    }

    bind() {
        let ids: Array<string> = []
        const ref = <Tree>this.$refs.tree
        let nodes: Array<any> = ref.getCheckedAndIndeterminateNodes()
        nodes.forEach(node => {
            ids.push(node.id)
        });

        api.Rbac.PostRbacRolebindroute(this.auth_id, ids, (resp: api.PostRbacRolebindrouteResponse) => {
            this.$Message.success(resp.errmsg);
            this.auth_id = ""
            this.auth = false
        })
    }

    showuser(id: string) {
        this.show_user_role_id = id
        console.log("showuser id---->", id)
        api.Rbac.GetRbacGetusersbyroleid(id, (resp: api.GetRbacGetusersbyroleidResponse) => {
            if (resp.data && resp.data.users) {
                this.userList = resp.data.users
                this.usertotal = resp.data.count
                this.isShowUser = true
                this.split1 = 0.5
            }
        })
    }

    edituser(row: Obj) {
        console.log("edituser row---->", row)
        this.edit1 = true
        this.isadduser = false
        this.userInfo.id = row.id
        this.userInfo.username = row.username
        this.userInfo.realname = row.realname
        this.userInfo.avatar = row.avatar
        this.userInfo.root = row.root.toString()
    }

    removeuser(id: string) {
        console.log("removeuser ---->", id)
        return () => {
            api.Rbac.GetRbacDeluser(id, (resp: api.GetRbacDeluserResponse) => {
                this.$Message.success(resp.errmsg);
                this.showuser(this.show_user_role_id)
            })
        }
    }

    userPage(page: number) {
        console.log("分页--->", page)
        this.userPageValue.page = page
        this.showuser(this.show_user_role_id)
    }

    userPageSize(limit: number) {
        console.log("分页 size---->", limit)
        this.userPageValue.limit = limit
        this.showuser(this.show_user_role_id)
    }

    addNewUser() {
        this.edit1 = true
        this.isadduser = true
        this.userInfo.id = ''
        this.userInfo.username = ''
        this.userInfo.realname = ''
        this.userInfo.avatar = ''
        this.userInfo.root = '0'
    }

    addHasUser() {
        console.log("addHasUser---->")
        this.has1 = true
        api.Rbac.PostRbacGetuserlist(this.allUserPageValue.limit, this.allUserPageValue.page, "", this.allUsersearchValue.username, (resp: api.PostRbacGetuserlistResponse) => {
            if (resp.data != undefined && resp.data.users != undefined && resp.data.count != undefined) {
                this.alluserlist = resp.data.users
                this.alluserlisttotal = resp.data.count
            }
        })
    }

    editUserOk() {
        if (this.isadduser) {
            api.Rbac.PostRbacRegister(this.userInfo.avatar, this.userInfo.password, this.userInfo.realname, parseInt(this.userInfo.root), this.userInfo.username, (resp: api.PostRbacRegisterResponse) => {
                if (resp.data) {
                    //todo: 角色用户绑定
                    api.Rbac.PostRbacUserbindrole(this.show_user_role_id, resp.data.id, (resp: api.PostRbacUserbindroleResponse) => {
                        this.$Message.success(resp.errmsg);
                        this.edit1 = false
                        this.showuser(this.show_user_role_id);
                    })
                }
            })
        } else {
            api.Rbac.PostRbacUpdateuser(this.userInfo.avatar, this.userInfo.realname, parseInt(this.userInfo.root), this.userInfo.id, (resp: api.PostRbacUpdateuserResponse) => {
                this.$Message.success(resp.errmsg);
                this.edit1 = false
                this.showuser(this.show_user_role_id);
            })
        }
    }

    editUserCancel() {
        console.log("editUserCancel---->")
        this.edit1 = false
        this.isadduser = false
        this.userInfo.id = ''
        this.userInfo.username = ''
        this.userInfo.realname = ''
        this.userInfo.avatar = ''
        this.userInfo.root = '0'
    }

    chooseAvatar(avatar: string) {
        this.userInfo.avatar = avatar
    }

    addUserOk() {
        console.log("addUserOk---->")
        api.Rbac.PostRbacRolebinduser(this.show_user_role_id, this.uids, (resp: api.PostRbacRolebinduserResponse) => {
            this.$Message.success(resp.errmsg);
            this.has1 = false
            this.showuser(this.show_user_role_id);
        })
    }

    addUserCancel() {
        console.log("addUserCancel---->")
        this.has1 = false
        this.uids = []
    }

    allusersearch() {
        this.allUserPageValue.page = 1
        this.allUserPageValue.limit = 10
        this.addHasUser()
    }

    alluserreset() {
        this.allUserPageValue.page = 1
        this.allUserPageValue.limit = 10
        this.allUsersearchValue = {
            username: '',
        }
        this.addHasUser()
    }

    selectchange(e: Array<any>) {
        console.log(e)
        e.forEach(element => {
            this.uids.push(element.id)
        });
        console.log("uids=", this.uids)
    }

    closeUserList() {
        this.show_user_role_id = ""
        this.userList = []
        this.usertotal = 0
        this.isShowUser = false
        this.split1 = 1
        this.userPageValue.page = 1
        this.userPageValue.limit = 10
    }
}