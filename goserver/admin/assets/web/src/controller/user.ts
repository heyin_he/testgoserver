import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import * as api from "devapi";
import { Message } from 'view-design';
import { resolvePtr } from 'dns';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class UserController extends Vue {

    modal1 : boolean =  false

    columns: Array<TableHeader> = [
        {
            title: '账号',
            slot: 'username',
            minWidth: 100
        },
        {
            title: '姓名',
            slot: 'realname',
            minWidth: 80
        },
        {
            title: '状态',
            slot: 'disable',
            minWidth: 100,
            filters: [
                {
                    label: '正常',
                    value: 0
                },
                {
                    label: '冻结',
                    value: 1
                }
            ],
            filterMultiple: false,
            filterMethod(value: number, row: any) {
                if (value === 1) {
                    return row.disable === 1;
                } else if (value === 0) {
                    return row.disable === 0;
                }
            }
        },
        {
            title: '时间',
            slot: 'created_at',
            minWidth: 120
        },
        {
            title: '操作',
            slot: 'action',
            width: 150,
            align: 'center'
        }
    ];

    total : number = 0
    data : Array<api.RbacUserListDataUserList> = []

    roleList: Array<api.RbacGetRoleList> = []

    formCustom : Obj = {
        id:"",
        username:'',
        realname: '',
        password:"",
        second_password:"",
        avatar: '',
        root: "0",
        role_id:"",
    }


    userRuleValidate: object = {
        username: [
            { required: true, message: '请输入账号', trigger: 'blur' },
            // { pattern: "/^(?=.*[a-zA-Z]+)(?=.*[0-9]+)[a-zA-Z0-9]{4,20}$/", message: '请输入4-20位数字和字母组成的账号', trigger: 'blur' }
        ],
        password: [
            { required: true, message: '请输入密码', trigger: 'blur'},
            // { pattern: "/^(?=.*[a-zA-Z]+)(?=.*[0-9]+)[a-zA-Z0-9]{6,20}$/", message: '请输入6-20位数字和字母组成的密码', trigger: 'blur'}
        ],
        second_password: [
            { required: true, message: '请再次输入密码', trigger: 'blur' },
            { validator: this.surePwd, trigger: 'blur' }
        ],
        realname: [
            { required: true, message: '请输入真实姓名', trigger: 'blur'}
        ],
    }

    avatars: Array<Obj> = [
        {
            "avatar": api.GlobalUnit.BASE_URL + "assets/avatar.png",
        },
        {
            "avatar": api.GlobalUnit.BASE_URL + "assets/avatar1.jpeg",
        },
        {
            "avatar": api.GlobalUnit.BASE_URL + "assets/avatar2.jpeg",
        },
        {
            "avatar": api.GlobalUnit.BASE_URL + "assets/avatar3.jpeg",
        }
    ]

    defaultList: Array<Obj> = []

    uploadList: Array<string> = []

    isadd:boolean = false

    searchValue: Obj = {
        username: '',
        realname: '',
        root:"0",
    }

    pageValue: NumberObj = {
        page: 1,
        limit: 10,
    }

    created() {
        this.getList()
        this.getRoleList()
    }

    getList() {
        api.Rbac.PostRbacGetuserlist(this.pageValue.limit, this.pageValue.page, this.searchValue.realname, this.searchValue.username, (resp: api.PostRbacGetuserlistResponse) => {
            if (resp.data != undefined && resp.data.users != undefined && resp.data.count != undefined) {
                this.data = resp.data.users
                this.total = resp.data.count
            }
        })
    }

    getRoleList(){
        api.Rbac.PostRbacGetroles(100, 1, "", "", (resp: api.PostRbacGetrolesResponse) => {
            if (resp.data != undefined && resp.data.roles != undefined) {
                this.roleList = resp.data.roles
            }
        })
    }


    surePwd(role:string, value:string, callback:Function){
        if(value == ""){
            callback(new Error("请再次输入密码!"))
        } else if (value != this.formCustom.password){
            callback(new Error("两次输入密码不一致!"))
        }else{
            callback()
        }
    }

    search() {
        this.getList()
    }

    reset() {
        this.searchValue = {
            username: '',
            realname: '',
            root: "0",
        }
        this.pageValue.page = 1
        this.pageValue.limit = 10
        this.getList()
    }

    show(row: Obj) {
        let id: string = row.id
        api.Rbac.GetRbacGetrolebyuserid(id, (resp: api.GetRbacGetrolebyuseridResponse)=>{
            if (resp.errcode == 0 && resp.data && resp.data.role){
                this.formCustom.role_id = resp.data.role.id
            }
            this.formCustom.id = row.id
            this.formCustom.username = row.username
            this.formCustom.realname = row.realname
            this.formCustom.avatar = row.avatar
            this.formCustom.root = row.root.toString()
            this.isadd = false
            this.modal1 = true
        }, true, false)
    }


    add() {
        this.isadd = true
        this.modal1 = true
        this.formCustom.username = ""
        this.formCustom.realname = ""
        this.formCustom.avatar = ""
        this.formCustom.password = ""
        this.formCustom.second_password = ""
        this.formCustom.root = "0"
        this.formCustom.role_id = ""
    }


    chooseAvatar(avatar : string){
        this.formCustom.avatar = avatar
    }

    remove(id: string) {
        return () => {
            console.log("id------>", id)
            api.Rbac.GetRbacDeluser(id, (resp:api.GetRbacDeluserResponse)=>{
                this.$Message.success(resp.errmsg);
                this.pageValue.page = 1
                this.pageValue.limit = 10
                this.getList();
            })
        }
    }

    ok() {
        let id: string = this.formCustom.id
        if (this.isadd){
            api.Rbac.PostRbacRegister(this.formCustom.avatar, this.formCustom.password, this.formCustom.realname, parseInt(this.formCustom.root), this.formCustom.username, (resp:api.PostRbacRegisterResponse)=>{
                //todo: 角色用户绑定
                if (resp.data) {
                    id = resp.data.id
                }
            })
        }else{
            api.Rbac.PostRbacUpdateuser(this.formCustom.avatar, this.formCustom.realname, parseInt(this.formCustom.root), this.formCustom.id)
        }
        if (this.formCustom.role_id != "" && id != ""){
            api.Rbac.PostRbacUserbindrole(this.formCustom.role_id, id, (resp: api.PostRbacUserbindroleResponse) => {
                this.$Message.success(resp.errmsg);
                this.modal1 = false
                this.pageValue.page = 1
                this.pageValue.limit = 10
                this.getList();
            })
        }
    }

    cancel() {
        this.isadd = false
        this.modal1 = false
        this.formCustom.username = ""
        this.formCustom.realname = ""
        this.formCustom.avatar = ""
        this.formCustom.password = ""
        this.formCustom.second_password = ""
        this.formCustom.root = "0"
        this.formCustom.role_id = ""
    }

    page(page:number){
        console.log("分页--->", page)
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number){
        console.log("分页 size---->", limit)
        this.pageValue.limit = limit
        this.getList()
    }
}