import { Component, Vue } from 'vue-property-decorator';
import CommonCardTitle from '@/components/CommonCardTitle.vue';
import { TableHeader } from '@/type';
import PopConfirm from '@/components/PopConfirm.vue';
import { getAgentChildList } from '@/api/member';
import store from '@/store';
interface Obj {
    [key: string]: string;
}

interface NumberObj {
    [key: string]: number;
}

@Component({
    components: { CommonCardTitle, PopConfirm }
})
export default class MemberController extends Vue {

    modal: boolean = false

    columns: Array<TableHeader> = [
        {
            title: '用户ID',
            slot: 'uid',
            minWidth: 80
        },
        {
            title: '昵称',
            slot: 'nickname',
            minWidth: 80
        },
        {
            title: '当前关卡',
            slot: 'max_level',
            minWidth: 80
        },
        {
            title: '服ID',
            slot: 'serverid',
            minWidth: 80
        }
    ];

    RuleValidate: object = {

    }

    total: number = 0
    data: Array<any> = []

    formCustom: Obj = {
    }

    searchValue: Obj = {
        uid: '',
    }

    pageValue: NumberObj = {
        page: 1,
        limit: 20,
    }

    created() {
        this.getList()
    }

    getList() {
        var param = { ...this.pageValue, ... {agent_id : parseInt((store.state as any).user.agent_id)}};
        getAgentChildList(param).then(resp => {
            if (resp.errcode == 0) {
                this.data = resp.data.list
                this.total = resp.data.total
            } else {
                this.$Message.error(resp.errmsg)
            }
        });
    }

    search() {

        this.getList()
    }

    reset() {
        this.pageValue.page = 1
        this.pageValue.limit = 20
        this.getList()
    }

    page(page: number) {
        console.log("分页--->", page)
        this.pageValue.page = page
        this.getList()
    }

    pageSize(limit: number) {
        console.log("分页 size---->", limit)
        this.pageValue.limit = limit
        this.getList()
    }

    show(row: any) {
        console.log("show row = ", row)
        this.modal = true
        this.formCustom.uid = row.uid
        this.formCustom.realname = row.nickname
    }

    //取消代理
    del(row: any) {
        console.log("del row = ", row)
        return () => {

        }
    }

    ok() {
        console.log("formCustom = ", this.formCustom)
    }

    cancel() {
        this.modal = false
    }

}
